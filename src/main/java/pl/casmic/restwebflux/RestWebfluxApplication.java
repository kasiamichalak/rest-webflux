package pl.casmic.restwebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude={MongoAutoConfiguration.class})
public class RestWebfluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestWebfluxApplication.class, args);
    }

}

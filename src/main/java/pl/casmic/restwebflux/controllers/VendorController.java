package pl.casmic.restwebflux.controllers;

import org.reactivestreams.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.casmic.restwebflux.domain.Vendor;
import pl.casmic.restwebflux.repositories.VendorRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class VendorController {

    public static final String BASE_URL = "/api/v1/vendors";
    private final VendorRepository vendorRepository;

    public VendorController(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
    }

    @GetMapping(BASE_URL)
    public Flux<Vendor> getAllVendors() {
        return vendorRepository.findAll();
    }

    @GetMapping(BASE_URL + "/{id}")
    public Mono<Vendor> getVendorById(@PathVariable String id) {
        return vendorRepository.findById(id);
    }

    @PostMapping(BASE_URL)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Void> createNewVendor(@RequestBody Publisher<Vendor> vendorStream) {
        return vendorRepository.saveAll(vendorStream).then();
    }

    @PutMapping(BASE_URL + "/{id}")
    public Mono<Vendor> updateVendor(@PathVariable String id, @RequestBody Vendor vendor) {
        vendor.setId(id);
        return vendorRepository.save(vendor);
    }

    @PatchMapping(BASE_URL + "/{id}")
    public Mono<Vendor> patchVendor(@PathVariable String id, @RequestBody Vendor vendor) {
        return vendorRepository.findById(id)
                .map(foundVendor -> {
                    if (foundVendor.getFirstname() != vendor.getFirstname()) {
                        foundVendor.setFirstname(vendor.getFirstname());
                        vendorRepository.save(foundVendor);
                    }
                    if (foundVendor.getLastname() != vendor.getLastname()) {
                        foundVendor.setLastname(vendor.getLastname());
                        vendorRepository.save(foundVendor);
                    }
                    return foundVendor;
                });
    }
}

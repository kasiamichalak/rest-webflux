package pl.casmic.restwebflux.bootstraps;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.casmic.restwebflux.domain.Category;
import pl.casmic.restwebflux.domain.Vendor;
import pl.casmic.restwebflux.repositories.CategoryRepository;
import pl.casmic.restwebflux.repositories.VendorRepository;

@Component
public class Bootstrap implements CommandLineRunner {

    CategoryRepository categoryRepository;
    VendorRepository vendorRepository;

    public Bootstrap(CategoryRepository categoryRepository,
                     VendorRepository vendorRepository) {
        this.categoryRepository = categoryRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if(categoryRepository.count().block() == 0) {
            System.out.println("---LOADING BOOTSTRAP DATA: CATEGORIES---");
            loadCategories();
        }
        if(vendorRepository.count().block() == 0) {
            System.out.println("---LOADING BOOTSTRAP DATA: VENDORS---");
            loadVendors();
        }
    }

    private void loadCategories() {
        categoryRepository.save(Category.builder().description("Fruits").build()).block();
        categoryRepository.save(Category.builder().description("Nuts").build()).block();
        categoryRepository.save(Category.builder().description("Breads").build()).block();
        categoryRepository.save(Category.builder().description("Meats").build()).block();
        categoryRepository.save(Category.builder().description("Eggs").build()).block();

        System.out.println("LOADED CATEGORIES: " + categoryRepository.count().block());
    }

    private void loadVendors() {
        vendorRepository.save(Vendor.builder().firstname("John").lastname("Boo").build()).block();
        vendorRepository.save(Vendor.builder().firstname("Johanna").lastname("Antropf").build()).block();
        vendorRepository.save(Vendor.builder().firstname("Bonzo").lastname("Gimzetti").build()).block();

        System.out.println("LOADED VENDORS: " + vendorRepository.count().block());
    }
}

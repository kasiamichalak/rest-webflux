package pl.casmic.restwebflux.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.casmic.restwebflux.domain.Category;

public interface CategoryRepository extends ReactiveMongoRepository<Category, String > {
}

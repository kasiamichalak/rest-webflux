package pl.casmic.restwebflux.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.casmic.restwebflux.domain.Vendor;

public interface VendorRepository extends ReactiveMongoRepository<Vendor, String> {
}

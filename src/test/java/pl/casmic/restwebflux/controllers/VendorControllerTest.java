package pl.casmic.restwebflux.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.casmic.restwebflux.domain.Vendor;
import pl.casmic.restwebflux.repositories.VendorRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class VendorControllerTest {

    WebTestClient webTestClient;
    VendorRepository vendorRepository;
    VendorController vendorController;

    @BeforeEach
    void setUp() {
        vendorRepository = Mockito.mock(VendorRepository.class);
        vendorController = new VendorController(vendorRepository);
        webTestClient = WebTestClient.bindToController(vendorController).build();
    }

    @Test
    void testGetAllVendors() {

        given(vendorRepository.findAll()).willReturn(Flux.just(
                Vendor.builder().firstname("firstname1").lastname("lastname1").build(),
                Vendor.builder().firstname("firstname2").lastname("lastname2").build()));

        webTestClient.get()
                .uri(VendorController.BASE_URL)
                .exchange()
                .expectBodyList(Vendor.class)
                .hasSize(2);
    }

    @Test
    void testGetVendorById() {

        given(vendorRepository.findById("someId"))
                .willReturn(Mono.just(Vendor.builder().firstname("firstname").lastname("lastname").build()));

        webTestClient.get()
                .uri(VendorController.BASE_URL +"/someId")
                .exchange()
                .expectBody(Vendor.class);
    }

    @Test
    void testCreateNewVendor() {

        given(vendorRepository.saveAll(any(Publisher.class)))
                .willReturn(Flux.just(Vendor.builder().build()));

        Mono<Vendor> vendorToCreate = Mono.just(Vendor.builder().firstname("firstname").lastname("lastname").build());

        webTestClient.post()
                .uri(VendorController.BASE_URL)
                .body(vendorToCreate, Vendor.class)
                .exchange()
                .expectStatus()
                .isCreated();
    }

    @Test
    void testUpdateVendor() {

        given(vendorRepository.save(any(Vendor.class)))
                .willReturn(Mono.just(Vendor.builder().build()));

        Mono<Vendor> vendorToUpdate = Mono.just(Vendor.builder().firstname("firstname").lastname("lastname").build());

        webTestClient.put()
                .uri(VendorController.BASE_URL + "/someid")
                .body(vendorToUpdate, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    void testPatchVendorWithFirstNameChanged() {

        given(vendorRepository.findById(anyString()))
                .willReturn(Mono.just(Vendor.builder().build()));

        given(vendorRepository.save(any(Vendor.class)))
                .willReturn(Mono.just(Vendor.builder().build()));

        Mono<Vendor> vendorToPatch = Mono.just(Vendor.builder().firstname("firstname").build());

        webTestClient.patch()
                .uri(VendorController.BASE_URL + "/someid")
                .body(vendorToPatch, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(vendorRepository).save(any());
    }

    @Test
    void testPatchVendorNoChanges() {

        given(vendorRepository.findById(anyString()))
                .willReturn(Mono.just(Vendor.builder().build()));

        given(vendorRepository.save(any(Vendor.class)))
                .willReturn(Mono.just(Vendor.builder().build()));

        Mono<Vendor> vendorToPatch = Mono.just(Vendor.builder().build());

        webTestClient.patch()
                .uri(VendorController.BASE_URL + "/someid")
                .body(vendorToPatch, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(vendorRepository, never()).save(any());
    }
}